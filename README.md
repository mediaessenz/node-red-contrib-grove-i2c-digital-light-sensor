This is a Node-RED node to get readings from the Grove Digital Light Sensor(https://wiki.seeedstudio.com/Grove-Digital_Light_Sensor/) connected to the Grove Base Hat for Raspberry Pi.

To get it working you have to install it in Node-RED at least ;-)

After this realize the new node "grove-i2c-digital-light-sensor" in the group "grove".
Drop it on your flow and add a trigger to its input and connect a debug node afterwards.
Happily watch the actualy calculated values of the sensor in a nice way. 

The rest is up to you!

The underlying python script, which is friendly picked from Cedric Maion https://github.com/cmaion/TSL2561, get's both the IR-reading and ambient reading from the sensor. It then scales the the readings and calculates the lux-value.

The included javascript handles the communication between Node-RED and the python script and hands over the values: lux ("msg.payload.lux"), visible light + infrared ("msg.payload.visibleWithInfrared"), infrared ("msg.payload.infrared"), gain ("msg.payload.gain") and timing ("msg.payload.timing").

January 2021
